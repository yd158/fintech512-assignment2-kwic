import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class KWIC {
    // extract title from the string input
    public static ArrayList<String> extractTitle(String input) {
        ArrayList<String> title = new ArrayList<String>();
        Scanner scan = new Scanner(input);

        while (scan.hasNextLine()) {
            String line = scan.nextLine();
            if (line.equals("::")) {
                break;
            }
        }
        while (scan.hasNextLine()) {
            String line2 = scan.nextLine();
            title.add(line2);
        }
        return title;
    }

    //extract ignored word from the string input
    public static ArrayList<String> extractIgnore(String input) {
        ArrayList<String> ignore = new ArrayList<String>();
        Scanner scan2 = new Scanner(input);
        while (scan2.hasNextLine()) {
            String line = scan2.nextLine();
            if (line.equals("::")) {
                break;
            } else {
                ignore.add(line);
            }
        }
        return ignore;
    }

    public static ArrayList<String> extractKeyword(ArrayList<String> input) {
        ArrayList<String> keyword = new ArrayList<>();
        ArrayList<String> ignore = extractIgnore(input);
        ArrayList<String> titles = extractTitle(input);

        for (int i=0; i<titles.size(); i++) {
            String title = titles.get(i);
            List l= Arrays.asList(title.split(" "));
            keyword.addAll(l);
            for (int j = 0; j < ignore.size(); j++) {
                for (int a = 0; a < keyword.size(); a++) {
                    if (ignore.get(j).equalsIgnoreCase(keyword.get(a))) {
                        keyword.remove(a);
                    }
                }
            }
        }

        Collections.sort(keyword);
        return keyword;
    }

    public static ArrayList<String> kwic(ArrayList<String> input) {
        ArrayList<String> output = new ArrayList<>();
        ArrayList<String> titles = extractTitle(input);
        ArrayList<String> keywords = extractKeyword(input);
        for (int i = 0; i < keywords.size(); i++) {

            String keyword = keywords.get(i);
            for (int j = 0; j < titles.size(); j++) {
                String title = titles.get(j);
                if (title.contains(keyword)) {
                    int count = 0;
                    Pattern pattern = Pattern.compile(keyword);
                    Matcher match = pattern.matcher( title );
                    while (match.find()) {
                        count++;
                    }

                    int i = title.indexOf(keyword);

                    ArrayList<Integer> num=new ArrayList<>();
                    num.add(i);
                    while (i >= 0) {
                        i = title.indexOf(keyword, i + 1);
                        num.add(i);
                    }

                    for(int c=0;c<num.size()-1;c++){
                        String s=num.get(c);

                        if(s!=0){
                            title = title.toLowerCase();
                            title = title.replace(keyword.toUpperCase(), keyword.toLowerCase());
                            title = title.substring(0,s)+keyword.toUpperCase()+title.substring(s+keyword.length(),title.length());
                        } else{
                            title = title.toLowerCase();
                            title = title.replaceFirst(keyword.toLowerCase(), keyword.toUpperCase());

                        }
                        output.add(title);

                    }

                }

            }

        }
        return output;
    }


    public static void main(String[] args) {
        ArrayList<String> input = new ArrayList<>();
        Scanner scan = new Scanner(System.in);

        while(scan.hasNextLine()){
            String line = scan.nextLine();
            if(line.equals("exit")){
                break;
            }
            input.add(line);
        }

        scan.close();
        ArrayList<String> output = kwic(input);

        for (int i=0;i<input.size(); i++) {
            System.out.println(output.get(i));
        }
    }

}

