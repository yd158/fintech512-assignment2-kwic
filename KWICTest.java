import java.util.*;
public class KWICTest {
        public ArrayList<String> set_input() {
            ArrayList<String>input=new ArrayList<>();
            input.add("is");
            input.add("the");
            input.add("of");
            input.add("and");
            input.add("as");
            input.add("a");
            input.add("but");
            input.add("::");
            input.add("Descent of Man");
            input.add("The Ascent of Man");
            input.add("The Old Man and The Sea");
            input.add("A Portrait of The Artist As a Young Man");
            input.add("A Man is a Man but Bubblesort IS A DOG");
            return input;
        }


        @Test
        void TestgetKeyword(){
            ArrayList<String>input=set_input();
            ArrayList<String>expected=new ArrayList<>();
            expected.add("Artist");
            expected.add("Ascent");
            expected.add("Bubblesort");
            expected.add("Descent");
            expected.add("DOG");
            expected.add("Man");
            expected.add("Old");
            expected.add("Portrait");
            expected.add("Sea");
            expected.add("Young");
            assertEquals(KWIC.getKeyword(input),expected);
        }

        @Test
        void Testoutput(){
            ArrayList<String>input=set_input();
            ArrayList<String>expected=new ArrayList<>();
            expected.add("a portrait of the ARTIST as a young man");
            expected.add("the ASCENT of man");
            expected.add("a man is a man but BUBBLESORT is a dog");
            expected.add("DESCENT of man");
            expected.add("a man is a man but bubblesort is a DOG");
            expected.add("descent of MAN");
            expected.add("the ascent of MAN");
            expected.add("the old MAN and the sea");
            expected.add("a portrait of the artist as a young MAN");
            expected.add("a MAN is a man but bubblesort is a dog");
            expected.add("a man is a MAN but bubblesort is a dog");
            expected.add("the OLD man and the sea");
            expected.add("a PORTRAIT of the artist as a young man");
            expected.add("the old man and the SEA");
            expected.add("a portrait of the artist as a YOUNG man");
            assertEquals(KWIC.Output(input),expected);

        }


    }
